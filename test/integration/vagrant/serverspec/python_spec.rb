require 'serverspec'
set :backend, :exec

describe package('apt'), :if => os[:family] == 'ubuntu' do
  it { should be_installed }
end

describe package('dnf'), :if => (os[:family] == 'redhat' or os[:family] == 'fedora')  do
  it { should be_installed }
end
