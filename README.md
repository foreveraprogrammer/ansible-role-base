## Purpose
This will test the ansible role which installs git and update all the packages in kitchen using vagrant. 

## Setup
To test roles using [kitchen](https://kitchen.ci/) you need to install the following programs:

* ruby-full
* vagrant

Run the following command in a terminal **sudo gem install bundler** to install the ruby bundler.

Create a file called Gemfile with the following text inside of it:

```
source "https://rubygems.org"

gem "test-kitchen"
gem "kitchen-vagrant"
gem "kitchen-ansible"
gem "serverspec"
gem "vagrant"
```

Then use the **sudo bungle install** command in terminal. You can also manually install each of these using **sudo gem install {program name}**.

## Using Kitchen
Below are some of the basic kitchen commands:

* **kitchen test** - creates, provisions and runs all the tests for an ansible playbook inside the VM
* **kitchen converge**  - creates and provisions the VM 
* **kitchen list** - list all VMs that are avaiable
* **kitchen verify** - runs the tests in the VM
* **kitchen login** - used to login into the VM
* **kitchen destroy** - destroy the VM
 

